from flask import Flask
import socket

app = Flask(__name__)
hostname = socket.gethostname()

@app.route('/')
def hello():
    return 'Python app #5 inside DOCKER container says "Hello World!" from "{}"'.format(hostname)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
