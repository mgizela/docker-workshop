# Docker poznámky

## Použitie na príklade Monga

### "Stiahnutie" Monga

```
$ docker pull mongo
Using default tag: latest
latest: Pulling from library/mongo
43c265008fae: Pull complete 
679a27ed88fa: Pull complete 
480a2b7cac89: Pull complete 
ab47cdcec495: Pull complete 
9256de55cd57: Pull complete 
ad5d4320ea8f: Pull complete 
210ffc04e209: Pull complete 
984cf767cb30: Pull complete 
7b184e5ab3fa: Pull complete 
Digest: sha256:61f688fbbca59038805d7dab094a6fe523beddb2a3ccffd119047a5d2d00511a
Status: Downloaded newer image for mongo:latest
```

### Spustenie v interaktívnom režime (terminál)

```
$ docker run -it --rm --name mongo -p 27017:27017 mongo
2016-10-25T04:40:14.176+0000 I CONTROL  [initandlisten] MongoDB starting : pid=1 port=27017 dbpath=/data/db 64-bit host=1f17913789f8
2016-10-25T04:40:14.176+0000 I CONTROL  [initandlisten] db version v3.2.10
2016-10-25T04:40:14.176+0000 I CONTROL  [initandlisten] git version: 79d9b3ab5ce20f51c272b4411202710a082d0317
2016-10-25T04:40:14.176+0000 I CONTROL  [initandlisten] OpenSSL version: OpenSSL 1.0.1t  3 May 2016
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten] allocator: tcmalloc
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten] modules: none
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten] build environment:
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten]     distmod: debian81
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten]     distarch: x86_64
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten]     target_arch: x86_64
2016-10-25T04:40:14.177+0000 I CONTROL  [initandlisten] options: {}
2016-10-25T04:40:14.186+0000 I STORAGE  [initandlisten] wiredtiger_open config: create,cache_size=18G,session_max=20000,eviction=(threads_max=4),config_base=false,statistics=(fast),log=(enabled=true,archive=true,path=journal,compressor=snappy),file_manager=(close_idle_time=100000),checkpoint=(wait=60,log_size=2GB),statistics_log=(wait=0),
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/enabled is 'always'.
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/defrag is 'always'.
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
2016-10-25T04:40:14.302+0000 I NETWORK  [HostnameCanonicalizationWorker] Starting hostname canonicalization worker
2016-10-25T04:40:14.302+0000 I FTDC     [initandlisten] Initializing full-time diagnostic data capture with directory '/data/db/diagnostic.data'
2016-10-25T04:40:14.327+0000 I NETWORK  [initandlisten] waiting for connections on port 27017
```

### "Pripojenie" sa na bežiace Mongo a spustenie klienta v tom istom kontajneri

```
$ docker exec -it mongo mongo
MongoDB shell version: 3.2.10
connecting to: test
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
	http://docs.mongodb.org/
Questions? Try the support group
	http://groups.google.com/group/mongodb-user
Server has startup warnings: 
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/enabled is 'always'.
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/defrag is 'always'.
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:40:14.301+0000 I CONTROL  [initandlisten] 
> show dbs
local  0.000GB
> 
```

### Pripojenie sa z lokálu (notebook) na kontajner

```
$ mongo
MongoDB shell version: 3.2.10
connecting to: test
Server has startup warnings: 
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] 
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/enabled is 'always'.
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] 
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/defrag is 'always'.
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2016-10-25T04:44:14.242+0000 I CONTROL  [initandlisten] 
> show dbs
local  0.000GB
> 
```

## Vymazanie ukončených kontajnerov

```
docker ps -q -f status=exited | xargs --no-run-if-empty docker rm
```

## Vymazanie nepoužívaných images

```
docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi
```

## Vymazanie nepoužívaných volumov

```
docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm
```

## Zobrazenie stats s názvami kontajnerov

```
docker ps --format={{.Names}} | xargs --no-run-if-empty docker stats
```

## Aliasy (Linux)

```
alias dc='docker-compose'
alias dim='docker images'
alias dps='docker ps -a'
alias drmc='docker ps -q -f status=exited | xargs --no-run-if-empty docker rm'
alias drmi='docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi'
alias drmv='docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm'
alias dst='docker ps --format={{.Names}} | xargs --no-run-if-empty docker stats'
alias drmongo='mkdir -p /ram/mongo/data; docker run --rm -it --name mongo -v /ram/mongo/data:/data/db -p 27017:27017 mongo --storageEngine wiredTiger'
alias drmongov='docker run --rm -it --name mongo -v mongo-data:/data/db -p 27017:27017 mongo --storageEngine wiredTiger'
alias drredis='mkdir -p /ram/redis/data; docker run --rm -it --name redis -v /ram/redis/data:/data/db -p 6379:6379 redis --appendonly yes'
```

## Odkazy

* [Docker Hub](https://hub.docker.com/)
* [Welcome to the Docs - Docker](https://docs.docker.com/)
* [Docker run reference - Docker](https://docs.docker.com/engine/reference/run/)
* [Dockerfile reference - Docker](https://docs.docker.com/engine/reference/builder/)
* [Intro to Docker](http://pointful.github.io/docker-intro/#/)
