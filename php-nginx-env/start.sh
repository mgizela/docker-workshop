#!/usr/bin/env bash
set -e

NORMAL=$(tput sgr0)
BOLD=$(tput bold)

# change ownership & rigths
chmod 755 /app
chown -R user:user /app

# run php & nginx
echo "Starting ${BOLD}DokuWiki${NORMAL}"
/usr/sbin/php5-fpm && /usr/sbin/nginx
