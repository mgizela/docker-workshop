from flask import Flask
from redis import Redis
import socket

app = Flask(__name__)
redis = Redis(host='redis', port=6379)
hostname = socket.gethostname()

@app.route('/')
def hello():
    redis.incr('hits')
    return 'Python app inside Docker container said "Hello World!" from "%s" %s times.' % (hostname, redis.get('hits'))

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
